#include<signal.h>
#include<string.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
int spawn(char* program, char** arg_list){
	pid_t child_pid;
	child_pid = fork();
	if(child_pid !=0)
		return child_pid;
	else{
		execvp(program,arg_list);
		fprintf(stderr, "an error ocurred in execvp\n");
		abort();
	}

}
		int main(){
		int child_status;

		char *arg_list[]={
		"ls",
		"-l",
		"/",
		NULL
		};
		spawn("ls", arg_list);
		wait(&child_status);
		if(WIFEXITED(child_status))
		printf("the child process  exite normally, with exit code %i\n",WEXITSTATUS(child_status));
		else
		printf("the child process is anormally\n");
		return 0;


		}
