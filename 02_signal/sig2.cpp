#include<signal.h>
#include<stdio.h>
#include<strings.h>
#include<sys/types.h>
#include<unistd.h>
sig_atomic_t senal1=0;


void handler(int signal_number){
++senal1;
}


int main(){
struct sigaction sa;
bzero(&sa,sizeof(sa));
sigaction(SIGUSR1,&sa,NULL);
sa.sa_handler=&handler;
printf("aaha %d\n", senal1);
return 0;
}
