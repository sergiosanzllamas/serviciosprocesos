#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<unistd.h>
#include<signal.h>
#include<sys/wait.h>

int papa(char *programa,char** lista){
	pid_t hijo_pid;
	hijo_pid=fork();
	if(hijo_pid !=0){
		return hijo_pid;
	}else{
		execvp(programa,lista);
		fprintf(stderr, "un error\n");
		abort();
	}
}
int main(){
	char *listaa[]={"ls","-l","/",NULL};
	int hijo_estas;
	papa("ls",listaa);
	wait(&hijo_estas);
	if(WIFEXITED(hijo_estas))
		printf("el codigo del proceso hijo va bien y es %d\n", WIFEXITED(hijo_estas));
	else
		printf("el proceso hijo va mal\n");

	return 0;
}

