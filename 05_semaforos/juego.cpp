#include<sys/ipc.h>
#include<semaphore.h>
#include<pthread.h>
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<time.h>
#include<sys/time.h>
#include<signal.h>
pthread_mutex_t ex=PTHREAD_MUTEX_INITIALIZER;
static pthread_key_t thread_log_key;
#define N 14
#define NPLAYS 4
sig_atomic_t a_cenar=0;
sig_atomic_t plays=0;
sem_t semaforo;
void inicializar(int nuplays){
	sem_init(&semaforo,0,4);
}
void *say(void *para){
	while(!a_cenar){

		sem_wait(&semaforo);
		pthread_mutex_lock(&ex);
		plays++;
		pthread_mutex_unlock(&ex);

		sleep(rand()%3+1);
		pthread_mutex_lock(&ex);

		plays--;
		pthread_mutex_unlock(&ex);

		sem_post(&semaforo);
		sleep(rand()%10+1);
	}
}

int main(int argc, char *argv[]){
	pthread_t ninio[N];
	struct timeval tv;
	time_t curtime;
	char tiempo[32];
	u_int8_t count=0xFF;
	srand(time(NULL));
	inicializar(NPLAYS);
	for(int i=0;i<N;i++)
		pthread_create(&ninio[i],NULL,&say,(void*)NULL);
	while(count){

		count--;
		gettimeofday(&tv,NULL);
		curtime=tv.tv_sec;
		strftime(tiempo,32,"%T",localtime(&curtime));
		printf(" [%s]usadas %i                             \r",tiempo, plays);
		fflush(stdout);
		usleep(500000);
	}
	a_cenar=1;
	for(int i=0;i<N;i++)
		pthread_join(ninio[i],NULL);
	sem_destroy(&semaforo);
	return EXIT_SUCCESS;

}







