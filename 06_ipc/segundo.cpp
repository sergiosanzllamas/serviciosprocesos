#include<stdio.h>
#include<stdlib.h>
#include<sys/stat.h>
#include<sys/shm.h>


int main(){
	int segment_id;
	char *shared_memory;
	struct shmid_ds shmbuffer;
	int segment_size;
	const int shared_segment_size=0x6400;
	//el id del segmento lo igualas para coger un segemento pones el iterproceso privado,el tamaño de segmento compartido,creas el interproceso,creas la ruptura del interproceso, das permisos.
	segment_id=shmget(IPC_CREAT,shared_segment_size,IPC_CREAT|IPC_EXCL|S_IRUSR|S_IWUSR);
	//del puntero de la memoria compratida haces una reserva de memoria con shmat con el id del segmento.
	shared_memory=(char*) shmat(segment_id,0,0);
	//sacas por pantalla ala direcccion de memoria del puntero.
	printf("shared memory attached address %p\n", shared_memory);
	//sacas la astadisticas delid del segmento y la metes en el buffer
	shmctl(segment_id,IPC_STAT,&shmbuffer);
	segment_size=shmbuffer.shm_segsz;
	printf("segment size: %d\n", segment_size);
	//sacas por pantalla el tamaño del segmento
	sprintf(shared_memory,"Hello World");
	//pintas en la memoria compartida un hola mundo
	shmdt(shared_memory);
	//pasas la memoria compartida
	shared_memory=(char*)shmat(segment_id,(void*)0x5000000,0);
	//amplias de reserva e memoria compartida
	printf("shared memory reattached address %p\n", shared_memory);
	printf("%s\n", shared_memory);
	//pintas la memoria compartida
	shmdt(shared_memory);
	shmctl(segment_id,IPC_RMID,0);








	return EXIT_SUCCESS;
}
