#include<sys/ipc.h>
#include<sys/sem.h>
#include<sys/types.h>
#include<stdio.h>

union semun{
	int val;
	struct semid_ds *buf;
	unsigned short int *array;
	struct seminfo *__buf;
};

int binary_semaforo_allocation(key_t key, int sem_flags){

	return semget(key, 1, sem_flags);
}

int binary_semaforo_deallocate(int semid){
	union semun ignored_argument;
	return semctl(semid,1,IPC_RMID);
}

int main()
{
	printf("%i\n",binary_semaforo_deallocate);
	printf("%c %i %s",semget);
	printf("%s %i \n", semctl);
	return 0;
}

