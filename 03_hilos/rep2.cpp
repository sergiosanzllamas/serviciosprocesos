#include<stdio.h>
#include<pthread.h>
struct caracter{
	char signo;
	int veces;
};
void *pinta(void*algo){
	struct caracter*p=(struct caracter *)algo;
	int i;
	for(i=0;i<p->veces;i++)
		fputc(p->signo,stderr);
	return NULL;

}

int main(){
	pthread_t hilo1;
	pthread_t hilo2;
	struct caracter hilo1_args;
	struct caracter hilo2_args;
	hilo1_args.signo='S';
	hilo1_args.veces=5;
	pthread_create(&hilo1,NULL,&pinta,&hilo1_args);
	hilo2_args.signo='M';
	hilo2_args.veces=6;
	pthread_create(&hilo2,NULL,&pinta,&hilo2_args);
	pthread_join(hilo1,NULL);
	pthread_join(hilo2,NULL);
printf("\n");
//programa anterior con el join de los hilos
	printf("Hilo 1: %c %i\n", hilo1_args.signo,hilo1_args.veces);
	printf("Hilo 2: %c %i\n", hilo2_args.signo,hilo2_args.veces);



	return 0;
}
