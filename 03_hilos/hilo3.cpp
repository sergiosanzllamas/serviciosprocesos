#include<stdio.h>

#include<pthread.h>
struct char_print_params{
	char caracter;
	int count;

};

void *char_print(void *parameters){
	struct char_print_params *p=(struct char_print_params*)parameters;
	for(int i=0;i<p->count;i++)
		fputc(p->caracter,stderr);

}


int main(){
	pthread_t thread1_id;

	pthread_t thread2_id;
	struct char_print_params thread1_args;
	struct char_print_params thread2_args;

	thread1_args.caracter='s';
	thread1_args.count=10;
	pthread_create(&thread1_id,NULL,&char_print,&thread1_args);
	thread2_args.caracter='m';
	thread1_args.count=20;
	pthread_create(&thread2_id,NULL,&char_print,&thread2_args);

	fputc(thread1_args.caracter,stderr);
	fputc(thread1_args.count,stderr);

	fputc(thread2_args.caracter,stderr);

	return 0;
}
